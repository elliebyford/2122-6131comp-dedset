// #include <esp32-hal.h>
// #include <HardwareSerial.h>
// #include <HttpClient.h>
// #include <IPAddress.h>
// #include <Esp32WifiManager.h>

// int requests = 0;
// // TODO: You'll need to change these!
// const char *SSID = "test";
// const char *PASS = "tester123";
// const int LED_PIN = 12;
// #define SERIAL_BAUD 115200

// // server
// WiFiServer server;

// // basic boilerplate to initialise wifi
// // and to flash the LED once after connection
// void setup()
// {
//     Serial.begin(SERIAL_BAUD);

//     Serial.print("Connecting to ");
//     Serial.println(SSID);
//     WiFi.begin(SSID, PASS);
//     while (WiFi.status() != WL_CONNECTED)
//     {
//         delay(250);
//         Serial.println(".");
//     }
//     Serial.print("Connected as :");
//     Serial.println(WiFi.localIP());

//     server.begin(80);

//     pinMode(LED_PIN, OUTPUT);
//     digitalWrite(LED_PIN, HIGH);
//     delay(1000);
//     digitalWrite(LED_PIN, LOW);
// }

// void sendHeaders(WiFiClient &client)
// {
//     client.println("HTTP/1.1 200 OK");
//     client.println("Content-type: text/html");
//     client.println("Connection: close");
//     client.println();
//     client.println("<!DOCTYPE html>");
// }

// void sendHTML(WiFiClient &client)
// {
//     client.println("<html>");
//     client.print("<head><title>ESP Web Server (");
//     client.print(requests);
//     client.println(")</title></head>");
//     client.println("<body>");
//     client.println("<h1> Welcome to the ESP server!</h1>");
//     client.print("<p> LED status ");
//     client.print(digitalRead(LED_PIN));
//     client.println("</p>");
//     client.println("</body>");
//     client.println("</html>");
//     client.println();
// }

// // handles the incoming request and alters the state of the LED pin
// void processRequest(const String &header)
// {
//     requests++;
//     if (header.indexOf("GET /led/on") != -1)
//         digitalWrite(LED_PIN, HIGH);
//     else if (header.indexOf("GET /led/off") != -1)
//         digitalWrite(LED_PIN, LOW);
//     else if (header.indexOf("GET /led/swap") != -1)
//         digitalWrite(LED_PIN, !digitalRead(LED_PIN));
// }

// //processing the incoming string
// //passing it to processRequest
// // sending the reponse back via sendHeaders and sendHTML
// void handleClient(WiFiClient &client)
// {
//     Serial.println(client.remoteIP());
//     String header;
//     header.reserve(512);
//     String currentLine;
//     currentLine.reserve(196);
//     while (client.connected())
//     {
//         char c = client.read();
//         if (c < 255)
//         {
//             Serial.print(c);
//             header.concat(c);
//             if (c == '\n')
//             {
//                 // end of request - send response
//                 if (currentLine.length() == 0)
//                 {
//                     processRequest(header);
//                     sendHeaders(client);
//                     sendHTML(client);
//                     break; // finished - break out
//                 }
//                 else // new line started
//                     currentLine = "";
//             }
//             else if (c != '\r')
//                 currentLine += c;
//         }
//         else //junk - break out
//             break;
//     }
// }

// void loop()
// {
//     WiFiClient client = server.available();
//     if (client)
//     {
//         delay(100); //wait for client to start
//         handleClient(client);
//         client.stop();
//         Serial.println("---Disconnected client");
//     }
// }

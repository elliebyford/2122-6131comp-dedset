#include <SD.h>
#include <SPIFFS.h>
#include <stdio.h>
#include <time.h>

class Card
{

public:
    const int CS_PIN = 5;
    const char *FILENAME = "/arduino.txt";

    void readData()
    {
        SD.begin(CS_PIN);

        File file = SD.open(FILENAME);

        while (file.available())
        {
            String readLine = file.readStringUntil('\r');
            readLine.trim();
            if (readLine.length() > 0)
            {
                Serial.println(readLine);
            }
        }

        file.close();
        SD.end();
    }

    void writeFile()
    {
        SD.begin(CS_PIN);
        Serial.println(F("Deleting/Opening file"));
        Serial.print(F("Deleting.. "));
        Serial.println(SD.remove(FILENAME));
        File file = SD.open(FILENAME, FILE_WRITE);
        if (file)
        {
            Serial.println(F("Writing file"));
            file.println("XD");
           
            file.println("XD");
            Serial.println(F("Closing file"));
            file.close();
        }
        else
        {
            Serial.println(F("File open write failed"));
        }
        Serial.println(F("Closing SDcard"));
        SD.end();
    }

    void setup_card()
    {

        Serial.println(F("Opening SD card"));

        writeFile();

        Serial.println(F("About to start reading!"));
        Serial.println();
        Serial.println();

        readData();
        Serial.println(F("Done!"));
    }

    void SaveSDoutput(double temperature, double humidity)
    {

        SD.begin(CS_PIN);
        time_t now;
        struct tm ts;
        char buf[80];
        // Get current time
        time(&now);
        Serial.println(F("Deleting/Opening file"));
        Serial.print(F("Deleting.. "));
        Serial.println(SD.remove(FILENAME));
        File file = SD.open(FILENAME, FILE_WRITE);
        if (file)
        {
            Serial.println(F("Writing file"));
            file.println("Current");
            ts = *localtime(&now);
            // Time format "ddd yyyy-mm-dd hh:mm:ss zzz"
            strftime(buf, sizeof(buf), "%a %Y-%m-%d %H:%M:%S %Z", &ts);
            file.printf("%s\n", buf);
            file.println("Temperature is: ");
            file.println(temperature);
            file.println("Humidity is: ");
            file.println(humidity);
            Serial.println(F("Closing file"));
            file.close();
        }
        else
        {
            Serial.println(F("File open write failed"));
        }
        Serial.println(F("Closing SDcard"));
        SD.end();
    }
};
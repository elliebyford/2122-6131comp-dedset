#include <SD.h>
#include <SPIFFS.h>
#include <ESP32Encoder.h>
#include <Arduino.h>
#include <ledsRGB.cpp> // ledsRGB class called
#include <rotary.cpp>
#include <vector_data.cpp>
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>
#include <TFT_eSPI.h>
#include "AnalogueMeter.h"
#include <screen.cpp>
#include <SDcard.cpp>
//#include <server.cpp>
#include <wifi.cpp>


enum State
{
  LED_BLUE,
  LED_RED
};

enum class SystemStates
{
  OK,
  TEMP_OUT,
  HUM_OUT,
  BOTH_OUT
};

enum class ConditionStates
{
  TEMP_BELOW,
  HUM_BELOW,
  TEMP_WITHIN,
  HUM_WITHIN,
  TEMP_AND_HUM_WITHIN,
  TEMP_ABOVE,
  HUM_ABOVE,
  BOTH_TEMP_AND_HUM_OUT
};

uint32_t updateTime = 0;
const int BACKLIGHT = 33;
const int LIGHTSENSOR = 34;
TFT_eSPI tft = TFT_eSPI();
int bright = 0;

Screen screen;
Card card;
Wifi wifi;

// Server server;

State current;
const int LED_PIN = LED_BUILTIN;
const int LED_DELAY = 250;
unsigned long lastChangeTime;
unsigned long lastOutputTime = 0;
unsigned long lastTime = 0;

//#define DHTPIN 14
// Digital pin connected to the DHT sensor
// Feather HUZZAH ESP8266 note: use pins 3, 4, 5, 12, 13 or 14 --
// Pin 15 can work but DHT must be disconnected during program upload.

// Uncomment the type of sensor in use:
//#define DHTTYPE DHT11 // DHT 11
//#define DHTTYPE    DHT22     // DHT 22 (AM2302)
//#define DHTTYPE    DHT21     // DHT 21 (AM2301)

// See guide for details on sensor wiring and usage:
//   https://learn.adafruit.com/dht/overview
const int DHT_PIN = 14;
// DHT_Unified dht(DHTPIN, DHTTYPE);
DHT dht(DHT_PIN, DHT11);
// calling ledsRBG class and pass pin number of each color.

ledsRGB rgb(27, 26, 25);
// call the class
rotary rotate;
Vectors v;
AnalogueMeter meter(&tft);

uint32_t delayMS = 5000;
double temperature = 0; // Current temperature
double humidity = 0;    // Current humidity
SystemStates state = SystemStates::OK;
ConditionStates condition = ConditionStates::TEMP_AND_HUM_WITHIN;

boolean timeDiff(unsigned long start, int specifiedDelay)
{
  return (millis() - start >= specifiedDelay);
}
void ledBlue()
{
  digitalWrite(rgb.PIN_BLUE_LED_RGB, HIGH);
  digitalWrite(rgb.PIN_RED_LED_RGB, LOW);
  digitalWrite(rgb.PIN_GREEN_LED_RGB, LOW);
  if (timeDiff(lastChangeTime, LED_DELAY))
    current = LED_RED;
}

void ledRed()
{
  digitalWrite(rgb.PIN_RED_LED_RGB, HIGH);
  digitalWrite(rgb.PIN_BLUE_LED_RGB, LOW);
  digitalWrite(rgb.PIN_GREEN_LED_RGB, LOW);

  if (timeDiff(lastChangeTime, LED_DELAY))
    current = LED_BLUE;
}

void setup()
{
  Serial.begin(115200);
  card.setup_card();
  // server.setup_server();
  // wifi.setup_wifi();
  tft.init();
  tft.setRotation(3);

  tft.fillScreen(TFT_BLACK);

  meter.analogMeter(); // Draw analogue meter

  updateTime = millis();
  // server.setup_server();
  screen.screen_set();
  pinMode(BACKLIGHT, OUTPUT);
  digitalWrite(BACKLIGHT, HIGH);
  ledcAttachPin(BACKLIGHT, 0);
  ledcSetup(0, 5000, 7);
  // call the set method of the rotary class
  rotate.set_up_encoder();

  pinMode(rgb.PIN_BLUE_LED_RGB, OUTPUT);
  pinMode(rgb.PIN_RED_LED_RGB, OUTPUT);
  pinMode(rgb.PIN_GREEN_LED_RGB, OUTPUT);
  // Initialize device.
  dht.begin();
  // Serial.println(F("DHTxx Unified Sensor Example"));
  // // Print temperature sensor details.
  // sensor_t sensor;
  // dht.temperature().getSensor(&sensor);
  // Serial.println(F("------------------------------------"));
  // Serial.println(F("Temperature Sensor"));
  // Serial.print(F("Sensor Type: "));
  // Serial.println(sensor.name);
  // Serial.print(F("Driver Ver:  "));
  // Serial.println(sensor.version);
  // Serial.print(F("Unique ID:   "));
  // Serial.println(sensor.sensor_id);
  // Serial.print(F("Max Value:   "));
  // Serial.print(sensor.max_value);
  // Serial.println(F("°C"));
  // Serial.print(F("Min Value:   "));
  // Serial.print(sensor.min_value);
  // Serial.println(F("°C"));
  // Serial.print(F("Resolution:  "));
  // Serial.print(sensor.resolution);
  // Serial.println(F("°C"));
  // Serial.println(F("------------------------------------"));
  // // Print humidity sensor details.
  // dht.humidity().getSensor(&sensor);
  // Serial.println(F("Humidity Sensor"));
  // Serial.print(F("Sensor Type: "));
  // Serial.println(sensor.name);
  // Serial.print(F("Driver Ver:  "));
  // Serial.println(sensor.version);
  // Serial.print(F("Unique ID:   "));
  // Serial.println(sensor.sensor_id);
  // Serial.print(F("Max Value:   "));
  // Serial.print(sensor.max_value);
  // Serial.println(F("%"));
  // Serial.print(F("Min Value:   "));
  // Serial.print(sensor.min_value);
  // Serial.println(F("%"));
  // Serial.print(F("Resolution:  "));
  // Serial.print(sensor.resolution);
  // Serial.println(F("%"));
  // Serial.println(F("------------------------------------"));
  // // Set delay between sensor readings based on sensor details.
  // delayMS = sensor.min_delay / 1000;
}

// int maxTemp = 15;
// int minTemp = 10;
// int minHumidity = 15;
// int maxHumidity = 20;

void set_led()
{
  switch (state)
  {
  case SystemStates::OK:
    digitalWrite(rgb.PIN_GREEN_LED_RGB, HIGH);
    digitalWrite(rgb.PIN_RED_LED_RGB, LOW);
    digitalWrite(rgb.PIN_BLUE_LED_RGB, LOW);
    if (timeDiff(lastChangeTime, LED_DELAY))
      break;
  case SystemStates::TEMP_OUT:
    digitalWrite(rgb.PIN_RED_LED_RGB, HIGH);
    digitalWrite(rgb.PIN_GREEN_LED_RGB, LOW);
    digitalWrite(rgb.PIN_BLUE_LED_RGB, LOW);
    break;
  case SystemStates::HUM_OUT:
    digitalWrite(rgb.PIN_RED_LED_RGB, LOW);
    digitalWrite(rgb.PIN_GREEN_LED_RGB, LOW);
    digitalWrite(rgb.PIN_BLUE_LED_RGB, HIGH);
    break;
  case SystemStates::BOTH_OUT:
    State old = current;
    switch (current)
    {
    case LED_RED:
      ledRed();
      break;

    case LED_BLUE:
      ledBlue();
      break;
    }

    if (old != current)
      lastChangeTime = millis();

    break;
  }
}

void output_states()
{
  Serial.println("OUTPUT");
  switch (condition)
  {
  case ConditionStates::TEMP_AND_HUM_WITHIN:
    Serial.println("Temperature and humidity are within range");
    break;
  case ConditionStates::TEMP_WITHIN:
    Serial.println("Temperature is within range");
    break;
  case ConditionStates::HUM_WITHIN:
    Serial.println("Humidity is within range");
    break;
  case ConditionStates::TEMP_BELOW:
    Serial.println("Temperature is below range");
    break;
  case ConditionStates::HUM_BELOW:
    Serial.println("Humidity is below range");
    break;
  case ConditionStates::TEMP_ABOVE:
    Serial.println("Temperature is above range");
    break;
  case ConditionStates::HUM_ABOVE:
    Serial.println("Humidity is above range");
    break;
  case ConditionStates::BOTH_TEMP_AND_HUM_OUT:
    Serial.println("Both humidity and temperature are out of range");
    break;
  }
}

// void transition_states() {
//   switch(condition) {
//   case
//   }
// }

void check_conditions()
{
  // call the methods to get the temperatures
  int maxTemp = rotate.get_max_temperatures();
  int minTemp = rotate.get_min_temperatures();
  int maxHum = rotate.get_max_humidity();
  int minHum = rotate.get_min_humidity();

  bool tempOk = true;
  bool humOk = true;

  bool tempWithin = true;
  bool humWithin = true;

  if (temperature <= maxTemp && temperature >= minTemp)
  {
    // condition state here
    condition = ConditionStates::TEMP_WITHIN;
    state = SystemStates::OK;
    // Serial.println("Temperature is within range");
    tempWithin = true;
  }
  else
  {
    tempOk = false;
    tempWithin = false;
    // Serial.println("Changed range from green to");
    if (temperature > maxTemp)
    {
      condition = ConditionStates::TEMP_ABOVE;
      // Serial.println("Temperature is above range");
      tempWithin = false;
    }
    else
    {
      condition = ConditionStates::TEMP_BELOW;
      // Serial.println("Temperature is below range");
      tempWithin = false;
    }
  }

  if (humidity <= maxHum && humidity >= minHum)
  {
    // condition = ConditionStates::HUM_WITHIN;
    // Serial.println("Humidity is within range");
    humWithin = true;
  }
  else
  {
    humOk = false;
    humWithin = false;
    if (humidity > maxHum)
    {
      condition = ConditionStates::HUM_ABOVE;
      // Serial.println("Humidity is above range");
      humWithin = false;
    }
    else
    {
      condition = ConditionStates::HUM_BELOW;
      // Serial.println("Humidity is below range");
      humWithin = false;
    }
    if (temperature >= minTemp && humidity >= minHum)
      // condition = ConditionStates::BOTH_TEMP_AND_HUM_OUT;
      humWithin = false;
    tempWithin = false;

    state = SystemStates::BOTH_OUT;
    // condition = ConditionStates::BOTH_TEMP_AND_HUM_OUT;

    {
    }
  }

  if (tempOk && humOk)
  {
    state = SystemStates::OK;
    condition = ConditionStates::TEMP_AND_HUM_WITHIN;
  }
  else
  {
    if (!tempOk && !humOk)
    {
      state = SystemStates::BOTH_OUT;
      condition = ConditionStates::BOTH_TEMP_AND_HUM_OUT;
    }
    else if (!tempOk)

      state = SystemStates::TEMP_OUT;
    // Serial.println("Temp is not ok");
    else
      state = SystemStates::HUM_OUT;
  }
}

// void plotOutput()
// {

//  meter.plotNeedle("temp", dht.readTemperature(), 0);
//  //meter.plotNeedle("rotary", rotate.rangeCheckEncoder(), 0);
// }

void plotOutput()
{
  switch (screen.switcher)
  {
  case Screen_Status::Normal:
    meter.plotNeedle("rotary", rotate.rangeCheckEncoder(), 0);
    break;
  case Screen_Status::HUMIDITY:
    meter.plotNeedle("hum", dht.readHumidity(), 0);
    break;
  case Screen_Status::TEMP:
    meter.plotNeedle("temp", dht.readTemperature(), 0);
    break;
  case Screen_Status::ROTARY:
    meter.plotNeedle("rotary", rotate.rangeCheckEncoder(), 0); // It takes between 2 and 14ms to replot the needle with zero delay
    break;
  }
}

void loop()
{
  // all this is for the screen
  int val = analogRead(LIGHTSENSOR);

  bright = map(val, 0, 4095, 127, 0);
  ledcWrite(0, bright);
  
  if(millis() - lastTime >= 10000){
    lastTime = millis();
     wifi.setup_wifi(temperature, humidity);
  }
  
  
  
  if (updateTime <= millis())
  {
    screen.checkButton();
    plotOutput();

    updateTime = millis() + 35; // Update meter every 35 milliseconds
  }

  check_conditions();
  set_led();

  // call the method to detect how many times the button is pressed
  rotate.loop_for_button();
  // check the condition of the button
  rotate.check_condition_rotary();

  // Delay between measurements.
  // delay(delayMS);
  // lastChangeTime = millis();

  if (millis() - lastOutputTime >= 5000)
  {

    lastOutputTime = millis();
    output_states();
    // only checking DHT every 5s make it check more frequently

    // temperature = event.temperature >>>>>> move it after dht.temperature().getEvent(&event);
    // otherwise it will skip the first loop of if statement

    // Get temperature event and print its value.
    // sensors_event_t event;
    // dht.temperature().getEvent(&event); ///>>>
    // temperature = event.temperature;
    temperature = dht.readTemperature();
    humidity = dht.readHumidity();
    Serial.print(F("Temperature: "));
     Serial.print("\t"); 
    Serial.print(temperature);
     Serial.print("\t"); 
    Serial.print(F("Humidity: "));
     Serial.print("\t"); 
    Serial.print(humidity);
     Serial.print("\t"); 
    card.SaveSDoutput(temperature, humidity);
    v.save_vector(temperature, humidity);
    //v.print_vector();
    // server.save_url(temperature, humidity);
    //  if (millis() - lastOutputTime >= 10000)
    // {
   
    //}
    // // State old = current;
    // if (isnan(event.temperature))
    // {

    //   Serial.println(F("Error reading temperature!"));
    // }
    // else
    // {

    //   Serial.print(F("Temperature: "));
    //   Serial.print(event.temperature);

    //   Serial.println(F("°C"));
    // }

    // // Get humidity event and print its value.
    // dht.humidity().getEvent(&event);
    // humidity = event.relative_humidity;
    // if (isnan(event.relative_humidity))

    // {

    //   Serial.println(F("Error reading humidity!"));
    // }
    // else
    // {
    //   Serial.print(F("Humidity: "));
    //   Serial.print(event.relative_humidity);
    //   Serial.println(F("%"));
    // }
    // if (old != current)
    //   lastChangeTime = millis();
  }
}

#include <ESP32Encoder.h>
#include <Arduino.h>
// enum class to know when to set the variables
enum class Rotary_State
{
    MAX_TEMP,
    MIN_TEMP,
    MAX_HUM,
    MIN_HUM,
    Normal,
};
// class of the rotary encoder, I am not using a constructur maybe someone can fix it
class rotary
{

public:
    // Rotary_a and b are for righ and left turn, rotary c is when you press the button
    const int ROTARY_A = 16;
    const int ROTARY_B = 17;
    const int Rotary_C = 13;
    ESP32Encoder encoder;
    // variable for the screen
    int encodercnt = 0;
    // count how many time you press a button to set the variables
    int count_times_button = 0;
    int buttonState = 0; // current state of the button
    // the last state
    int lastButtonState = 0;
    // array that saves the values array index 0 is max and index 1 is min
    int tempetarues[2] = {25, 10};
    // temporary value of the temperature
    int temp_value = 0;
    // array that saves the values of humidity in array index 0 is max and index 1 is min
    int humidity[2] = {100, 0};
    // we initialize the status of the butto of normal, depending on how many times you press a button the state will change
    Rotary_State status = Rotary_State::Normal;

    void set_up_encoder()
    {
        // we set up the encoder
        ESP32Encoder::useInternalWeakPullResistors = UP;
        pinMode(Rotary_C, INPUT_PULLUP);
        encoder.attachHalfQuad(ROTARY_A, ROTARY_B);
        // set the counter to start from 15
        encoder.setCount(15);
    }

    // range limits and checks the encoder this is for the screen
    int rangeCheckEncoder()
    {

        if (encodercnt != encoder.getCount())
        {
            encodercnt = encoder.getCount();

            if (encodercnt >= 100)
            {
                encodercnt = 100;
                encoder.setCount(encodercnt);
            }
            else if (encodercnt <= 0)
            {
                encodercnt = 0;
                encoder.setCount(encodercnt);
            }
        }
        return encodercnt;
    }

    // this will check the status of the button and call the methods depending on the current status
    void check_condition_rotary()
    {
        switch (status)
        {
        case Rotary_State::Normal:
            rangeCheckEncoder();
            break;

        case Rotary_State::MAX_TEMP:
            set_max_temperature();
            break;
        case Rotary_State::MIN_TEMP:
            set_min_temperature();
            break;
        case Rotary_State::MAX_HUM:
            set_max_humidity();
            break;
        case Rotary_State::MIN_HUM:
            set_min_humidity();
        }
    }
    // with this we set the max
    int set_max_temperature()
    {
        // if the button have been press 3 times we will save the temperature on the array
        // we change the status of the button back to normal
        // and we enforce the encoder to start the next time from the current temperature
        if (count_times_button == 3)
        {
            Serial.println("This is your max temperature");
            tempetarues[0] = temp_value;
            encoder.setCount(tempetarues[0]);
            Serial.println(tempetarues[0]);

            status = Rotary_State::Normal;
        }
        // if the temp value is different than encoder we will execute the following code
        if (temp_value != encoder.getCount())
        {
            // save the new value
            temp_value = encoder.getCount();
            // this will force the temperature to not be greater than 30 and less than 5
            if (temp_value >= 30)
            {
                temp_value = 30;
                encoder.setCount(temp_value);
            }
            else if (temp_value <= 5)
            {
                temp_value = 5;
                encoder.setCount(temp_value);
            }

            Serial.println(temp_value);
        }
    }
    // set the min temperature
    int set_min_temperature()
    {
        // if the burron pressed 5 times save the temperature
        // change the status of the button
        // set the counter of the button to start again
        if (count_times_button == 5)
        {
            Serial.println("Minimun temperature");
            tempetarues[1] = temp_value;
            // set the counter back to 0
            //  count_times_button = 0;
            encoder.setCount(tempetarues[1]);

            status = Rotary_State::Normal;
        }

        if (temp_value != encoder.getCount())
        {
            temp_value = encoder.getCount();
            // in this case the limit of the temperature must be the max temperature - 1
            // Like that the minimun temperature will always be less than the max.
            if (temp_value >= tempetarues[0] - 1)
            {
                temp_value = tempetarues[0] - 1;
                encoder.setCount(temp_value);
            }
            else if (temp_value <= 5)
            {
                temp_value = 5;
                encoder.setCount(temp_value);
            }

            Serial.println(temp_value);
        }
    }
    // get the temperatures
    int get_max_temperatures()
    {
        return tempetarues[0];
    }

    int get_min_temperatures()
    {
        return tempetarues[1];
    }

    // get the hum
    int get_max_humidity()
    {
        return humidity[0];
    }

    int get_min_humidity()
    {
        return humidity[1];
    }
    // this will check if the button have been pressed or not
    void loop_for_button()
    {
        buttonState = digitalRead(Rotary_C);
        if (buttonState != lastButtonState)
        {
            // if the state has changed, increment the counter
            if (buttonState == HIGH)
            {
                // if the current state is HIGH then the button went from off to on:

                count_times_button++;

                // if button pressed 2 times, we set the status to max
                if (count_times_button == 2)
                {
                    status = Rotary_State::MAX_TEMP;
                }
                // if button pressed 4 times, we set the status to min
                if (count_times_button == 4)
                {
                    status = Rotary_State::MIN_TEMP;
                }

                // if button pressed 6 times, we set the status to max
                if (count_times_button == 6)
                {
                    status = Rotary_State::MAX_HUM;
                }
                // if button pressed 8 times, we set the status to min
                if (count_times_button == 8)
                {
                    status = Rotary_State::MIN_HUM;
                }

                Serial.println("on");
                Serial.print("number of button pushes: ");
                Serial.println(count_times_button);
            }

            else
            {
                // if the current state is LOW then the button went from on to off:
                Serial.println("off");
            }
            // Delay a little bit to avoid bouncing
            delay(50);
        }
        // save the current state as the last state, for next time through the loop
        lastButtonState = buttonState;
    }

    // with this we set the max
    int set_max_humidity()
    {
        // if the button have been press 3 times we will sae the temperature on the array
        // we change the status of the button back to normal
        // and we enforce the encoder to start the next time from the current temperature
        if (count_times_button == 7)
        {
            Serial.println("This is your max humidity");
            humidity[0] = temp_value;
            encoder.setCount(humidity[0]);
            Serial.println(humidity[0]);

            status = Rotary_State::Normal;
        }
        // if the temp value is different than encoder we will execute the following code
        if (temp_value != encoder.getCount())
        {
            // save the new value
            temp_value = encoder.getCount();
            // this will force the temperature to not be greater than 30 and less than 5
            if (temp_value >= 100)
            {
                temp_value = 100;
                encoder.setCount(temp_value);
            }
            else if (temp_value <= 0)
            {
                temp_value = 0;
                encoder.setCount(temp_value);
            }

            Serial.println(temp_value);
        }
    }

    // set the min temperature
    int set_min_humidity()
    {
        // if the burron pressed 5 times save the temperature
        // change the status of the button
        // set the counter of the button to start again
        if (count_times_button == 9)
        {
            Serial.println("Minimun humdity");
            humidity[1] = temp_value;
            // set the counter back to 0
            count_times_button = 0;
            encoder.setCount(humidity[1]);

            status = Rotary_State::Normal;
        }

        if (temp_value != encoder.getCount())
        {
            temp_value = encoder.getCount();
            // in this case the limit of the temperature must be the max temperature - 1
            // Like that the minimun temperature will always be less than the max.
            if (temp_value >= humidity[0] - 1)
            {
                temp_value = humidity[0] - 1;
                encoder.setCount(temp_value);
            }
            else if (temp_value <= 0)
            {
                temp_value = 0;
                encoder.setCount(temp_value);
            }

            Serial.println(temp_value);
        }
    }
};
<?php
    $group = $_GET['group'];
    $temp = $GET['t'];
    $hum = $GET['h'];

    $tz = 'Europe/London';
    $timestamp = time();
    $dt = new DateTime("now", new DateTimeZone($tz));
    $dt->setTimestamp($timestamp);
    $output = $dt->format('d.m.Y, H:i:s') . " t- " . $temp ." h- " . $hum . "\n";
    file_put_contents($group.".txt", $output, FILE_APPEND );
?>
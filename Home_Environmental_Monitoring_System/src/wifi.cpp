#include <HTTPClient.h>
#include <HardwareSerial.h>
#include <IPAddress.h>
#include <WString.h>
#include <WiFi.h>
#include <WiFiClient.h>
#include <WiFiType.h>

#define LED 26

class Wifi
{

    // TODO: You'll need to change these!
    const char *SSID = "iPhone";
    const char *PASS = "12345678";
    #define SERIAL_BAUD 115200
    const char *HOST = "";
    const int TIMEOUT = 10000;
    unsigned long storedReading = 44;

#define USE_HTTPCLIENT true

    /**
     * Do not the HTTPClient library - this means we do a lot of string parsing
     */
    void nonHTTPClient(String &url)
    {
        WiFiClient client; // @suppress("Abstract class cannot be instantiated")
        if (!client.connect(HOST, 80))
        {
            Serial.println("connection failed");
            return;
        }

        Serial.print("GET ");
        Serial.print(HOST);
        Serial.println(url);

        client.print("GET ");
        client.print(url);
        client.println(" HTTP/1.0");
        client.print("Host: ");
        client.println(HOST);
        client.println();

        long timing = millis();
        while (client.available() == 0)
        {
            // wait for response..
            if (millis() - timing > TIMEOUT)
            {
                Serial.println("Timed out waiting for client");
                client.stop();
                break;
            }
            else
                delay(100);
        }

        char buff[200];
        while (client.available())
        {
            // get response and dump to console
            int len = client.readBytes(buff, 199);
            buff[len] = '\0';
            Serial.print(buff);
        }
        if (client.connected())
            client.stop();

        Serial.println("---------");
    }

    /**
     * Uses the HTTPClient library - this simplifies header acquisition
     */
    void httpClient(String &url)
    {
        // requires HTTPClient library, depends WiFiClientSecure
        // in Sloeber, this requires alteration to platform.txt on ESP32 on Windows - escaping a
        // preprocessor string:-
        // compiler.cpreprocessor.flags=-DESP_PLATFORM -DMBEDTLS_CONFIG_FILE="\"mbedtls/esp_config.h\""
        // in:-
        // \sloeber\arduinoPlugin\packages\esp32\hardware\esp32\<version>\platform.txt
        // should compile without issue in Arduino IDE or PIO (tested on VSCode)
        Serial.println("Now using HTTPClient");

        HTTPClient hClient;
        // hClient.setConnectTimeout()
        // hClient.setTimeout()
        String fullURL = "https://cmp-dedset.azurewebsites.net";
        fullURL.concat(HOST);
        fullURL.concat(url);
        hClient.begin(fullURL);

        const char *headers[] = {"Date", "Server"};
        hClient.collectHeaders(headers, 2);
        int retCode = hClient.GET();

        if (retCode > 0)
        { // a real HTTP code
            Serial.print("HTTP ");
            Serial.println(retCode);
            if (retCode == HTTP_CODE_OK || retCode == HTTP_CODE_FOUND)
            {
                Serial.println("------");
                Serial.println("Date = " + hClient.header("Date"));
                Serial.println("Server = " + hClient.header("Server"));
                Serial.println("-------");
                Serial.println(hClient.getString());
            }
        }
        else
        {
            Serial.println("Error... ");
            Serial.println(HTTPClient::errorToString(retCode));
        }
    }

    public: 
    
    void setup_wifi(double temperature, double humidity)
    {
        pinMode(LED, OUTPUT);
        Serial.begin(SERIAL_BAUD);

        // connecting to the wifi
        Serial.print("Connecting to ");
        Serial.println(SSID);
        WiFi.begin(SSID, PASS);
        while (WiFi.status() != WL_CONNECTED)
        {
            delay(250);
            Serial.println(".");
        }
        Serial.print("Connected as :");
        Serial.println(WiFi.localIP());

        // query parameters to search for esp32 in last month
        // String id = "q";
        // String val = "esp32";
        // String id2 = "as_qdr";
        // String val2 = "m";
        // String url = "/search?";
        // url.concat(id);
        // url.concat("=");
        // url.concat(val);
        // url.concat("&");
        // url.concat(id2);
        // url.concat("=");
        // url.concat(val2);

        //   double temperature = 15; // Current temperature
        //  double humidity = 15;    // Current humidity
        String url = "/batch.php?";
        url.concat("storedReading");
        url.concat("=");
        url.concat(storedReading);
        url.concat("&");
        url.concat("temp");
        url.concat("=");
        url.concat(temperature);
        url.concat("&");
        url.concat("humidity");
        url.concat("=");
        url.concat(humidity);
        Serial.println(url);

        if (!USE_HTTPCLIENT)
        {
            nonHTTPClient(url);
        }
        else
        {
            httpClient(url);
        }
        {
            // if (millis() - storedReading >= 10000)
            // {
            //     storedReading = millis();
            // }
            // digitalWrite(LED, HIGH);
            // delay(250);
            // digitalWrite(LED, LOW);
            // delay(150);
        }
    }
};
//#include <Arduino.h>

class ledsRGB
{

    /// making class public to make it available in other class and call it when it need it
public:
    //creating properties
    int PIN_RED_LED_RGB;
    int PIN_BLUE_LED_RGB;
    int PIN_GREEN_LED_RGB;

    // creat constructor and to construct the object of ledsRGB class.
    // passing obejcts to constructor as paramters.
    ledsRGB(int redRGb, int greenRGb, int blueRGb)
    {

        // assign properties with parameters
        PIN_RED_LED_RGB = redRGb;
        PIN_BLUE_LED_RGB = blueRGb;
        PIN_GREEN_LED_RGB = greenRGb;
    }
};
# 2122-6131COMP-DEDSET



## Getting started
To start, you need to cd to the file Home-Environmental_Monitoring_System. Then, type
git checkout final-version.

## Run the code
You will need to make sure that you have installed the libraries that are on the platformio.ini.
On the wifi class you have to put your cell phone name and the password of the wifi or your phone. On the computer that you are working you will need to share wifi with your phone.
Because of the bug with the connection. To test it you will need to wait some seconds when the wifi is either connection or disconnecting (when this happen the programm will be froze). So just wait a few second and the programm will continue executing. 
For the rotary encoder you will need to press 2 times(when you run the code for first time the encoder button will be set up as if you pressed 1 time, therefore for the first time you only need to press the button once, in order to change the temperature. ) in order to set up the maximun temperature. After you select a temperature you need to press it again (3 time) in order to save it. Then if you press for the fourth time (you will change now the minimum). For the humidity it will be for the sixth time and so on. After you save the minimun humidity the state of the button will reset, therefore you change the variables again.
For the screen only press the normal button and it will change the mode.
Don't forget to have the same pin numbers on your hardware.


members

ellie byford (ellie.byford@hotmail.com) 

Saul Vilchez (svilchez010@gmail.com)

musa salih (musasaleih@gmail.com)

Macy Escho (macyesho@icloud.com)

Rebecca Ellison (@rebecca_ellison)
